package sh.surge.enksoftware.automatuum.blocks;

import sh.surge.enksoftware.automatuum.AutomatuumTab;
import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;

public class AmethyteBlock extends Block
{
    public AmethyteBlock()
    {
        super(Material.IRON);
        setSoundType(SoundType.METAL);
        setHarvestLevel("pickaxe", 6);
        setHardness(12.5F);
        setResistance(44.0F);
        setRegistryName("amethyte_block");
        setTranslationKey("automatuum.amethyte_block");
        setCreativeTab(AutomatuumTab.AUTOMATUUM_TAB);
    }
}
