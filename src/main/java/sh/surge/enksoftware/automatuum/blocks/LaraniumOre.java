package sh.surge.enksoftware.automatuum.blocks;

import sh.surge.enksoftware.automatuum.AutomatuumTab;
import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;

public class LaraniumOre extends Block
{
    public LaraniumOre()
    {
        super(Material.ROCK);
        setSoundType(SoundType.STONE);
        setHardness(8.0F);
        setResistance(15.0F);
        setHarvestLevel("pickaxe", 7);
        setRegistryName("laranium_ore");
        setTranslationKey("automatuum.laranium_ore");
        setCreativeTab(AutomatuumTab.AUTOMATUUM_TAB);
    }
}
