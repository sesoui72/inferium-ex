package sh.surge.enksoftware.automatuum.blocks;

import sh.surge.enksoftware.automatuum.AutomatuumTab;
import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;

public class HearaniumBlock extends Block
{
    public HearaniumBlock()
    {
        super(Material.IRON);
        setSoundType(SoundType.METAL);
        setHardness(11.5F);
        setResistance(40.0F);
        setHarvestLevel("pickaxe", 4);
        setRegistryName("hearanium_block");
        setTranslationKey("automatuum.hearanium_block");
        setCreativeTab(AutomatuumTab.AUTOMATUUM_TAB);
    }
}
