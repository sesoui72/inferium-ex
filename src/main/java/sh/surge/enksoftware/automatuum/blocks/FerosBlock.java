package sh.surge.enksoftware.automatuum.blocks;

import sh.surge.enksoftware.automatuum.AutomatuumTab;
import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;

public class FerosBlock extends Block
{
    public FerosBlock()
    {
        super(Material.IRON);
        setSoundType(SoundType.METAL);
        setHardness(15.0F);
        setResistance(50.0F);
        setHarvestLevel("pickaxe", 8);
        setRegistryName("feros_block");
        setTranslationKey("automatuum.feros_block");
        setCreativeTab(AutomatuumTab.AUTOMATUUM_TAB);
    }
}
