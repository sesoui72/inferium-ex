package sh.surge.enksoftware.automatuum.blocks;

import sh.surge.enksoftware.automatuum.AutomatuumTab;
import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;

public class HearaniumOre extends Block
{
    public HearaniumOre()
    {
        super(Material.ROCK);
        setSoundType(SoundType.STONE);
        setHardness(6.5F);
        setResistance(10.0F);
        setHarvestLevel("pickaxe", 4);
        setRegistryName("hearanium_ore");
        setTranslationKey("automatuum.hearanium_ore");
        setCreativeTab(AutomatuumTab.AUTOMATUUM_TAB);
    }
}
