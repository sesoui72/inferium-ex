package sh.surge.enksoftware.automatuum.blocks;

import sh.surge.enksoftware.automatuum.AutomatuumTab;
import sh.surge.enksoftware.automatuum.items.ItemRegistry;
import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.MathHelper;

import java.util.Random;

public class FerosOre extends Block
{
    public FerosOre()
    {
        super(Material.ROCK);
        setSoundType(SoundType.STONE);
        setHardness(6.0F);
        setResistance(12.0F);
        setHarvestLevel("pickaxe", 8);
        setRegistryName("feros_ore");
        setTranslationKey("automatuum.feros_ore");
        setCreativeTab(AutomatuumTab.AUTOMATUUM_TAB);
    }

    @Override
    public Item getItemDropped(IBlockState state, Random random, int par3)
    {
        return new ItemStack(ItemRegistry.FEROS_CHUNK).getItem();
    }

    @Override
    public int quantityDropped(Random random)
    {
        return MathHelper.getInt(random, 1, 5);
    }
}
