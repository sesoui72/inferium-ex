package sh.surge.enksoftware.automatuum.gen;

import sh.surge.enksoftware.automatuum.Automatuum;
import sh.surge.enksoftware.automatuum.AutomatuumConfig;
import sh.surge.enksoftware.automatuum.blocks.BlockRegistry;
import sh.surge.enksoftware.automatuum.fluid.FluidRegistry;
import net.minecraft.init.Blocks;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.chunk.IChunkProvider;
import net.minecraft.world.gen.IChunkGenerator;
import net.minecraft.world.gen.feature.WorldGenLakes;
import net.minecraft.world.gen.feature.WorldGenMinable;
import net.minecraftforge.fml.common.IWorldGenerator;
import org.apache.logging.log4j.Level;

import java.util.Random;

public class GenerationHandler implements IWorldGenerator
{
    @Override
    public void generate(Random random, int posX, int posZ, World world, IChunkGenerator chunkGenerator, IChunkProvider chunkProvider)
    {
        switch (world.provider.getDimension())
        {
            case -1:
                generateFeros(world, random, posX * 16, posZ * 16);
                break;
            case 0:
                generateLaranium(world, random, posX * 16, posZ * 16);
                generateAmethyte(world, random, posX * 16, posZ * 16);
                generateToparite(world, random, posX * 16, posZ * 16);
                generateHearanium(world, random, posX * 16, posZ * 16);
                generateSapphirite(world, random, posX * 16, posZ * 16);
                generateCrystal(world, random, posX * 16, posZ * 16);
                generateCrudeOil(world, random, posX * 16, posZ * 16);
                break;
            case 1:
                break;
        }
    }

    // Ore Generation starts here!
    // Do not put any fluids into this section EnK... you'll regret it afterwards.

    private void generateLaranium(World world, Random rand, int chunkX, int chunkZ)
    {
        if (AutomatuumConfig.OREGEN.oreGenLaranium) // Check if the ore is enabled to be generated...
        {
            for (int k = 0; k < 4; k++) // Set max spawn tries to 4
            {
                int firstXCoord = chunkX + rand.nextInt(16); // Calculate in which Chunk on the X-Axis the ore should be generated
                int firstZCoord = chunkZ + rand.nextInt(16); // Calculate in which Chunk on the Z-Axis the ore should be generated

                int getPosY = rand.nextInt(9); // Get a random Y-Axis position for the ore
                BlockPos orePos = new BlockPos(firstXCoord, getPosY, firstZCoord); // Calculate a new Block Position for the ore
                (new WorldGenMinable(BlockRegistry.LARANIUM_ORE.getDefaultState(), 8)).generate(world, rand, orePos);

                if (AutomatuumConfig.GENERAL.oreFluidLogger) // If Ore/Fluid Generation Logging is active...
                {
                    Automatuum.LOGGER.log(Level.DEBUG, "Generated laranium ore at chunks [" + firstXCoord + ", " + firstZCoord + "] at height Y: " + getPosY);
                }
            }
        }
    }

    private void generateAmethyte(World world, Random rand, int chunkX, int chunkZ)
    {
        if (AutomatuumConfig.OREGEN.oreGenAmethyte) // Check if the ore is enabled to be generated...
        {
            for (int k = 0; k < 7; k++) // Set max spawn tries to 7
            {
                int xChunk = chunkX + rand.nextInt(16); // Calculate in which Chunk on the X-Axis the ore should be generated
                int zChunk = chunkZ + rand.nextInt(16); // Calculate in which Chunk on the Z-Axis the ore should be generated

                int getPosY = rand.nextInt(26); // Get a random Y-Axis position for the ore
                BlockPos orePos = new BlockPos(xChunk, getPosY, zChunk); // Calculate a new Block Position for the ore
                (new WorldGenMinable(BlockRegistry.AMETHYTE_ORE.getDefaultState(), 8)).generate(world, rand, orePos);

                if (AutomatuumConfig.GENERAL.oreFluidLogger) // If Ore/Fluid Generation Logging is active...
                {
                    Automatuum.LOGGER.log(Level.DEBUG, "Generated amethyte ore at chunks [" + xChunk + ", " + zChunk + "] at height Y: " + getPosY);
                }
            }
        }
    }

    private void generateToparite(World world, Random rand, int chunkX, int chunkZ)
    {
        if (AutomatuumConfig.OREGEN.oreGenToparite) // Check if the ore is enabled to be generated...
        {
            for (int k = 0; k < 9; k++) // Set max spawn tries to 9
            {
                int xChunk = chunkX + rand.nextInt(16); // Calculate in which Chunk on the X-Axis the ore should be generated
                int zChunk = chunkZ + rand.nextInt(16); // Calculate in which Chunk on the Z-Axis the ore should be generated

                int getPosY = rand.nextInt(35); // Get a random Y-Axis position for the ore
                BlockPos orePos = new BlockPos(xChunk, getPosY, zChunk); // Calculate a new Block Position for the ore
                (new WorldGenMinable(BlockRegistry.TOPARITE_ORE.getDefaultState(), 10)).generate(world, rand, orePos);

                if (AutomatuumConfig.GENERAL.oreFluidLogger) // If Ore/Fluid Generation Logging is active...
                {
                    Automatuum.LOGGER.log(Level.DEBUG, "Generated toparite ore at chunks [" + xChunk + ", " + zChunk + "] at height Y: " + getPosY);
                }
            }
        }
    }

    private void generateHearanium(World world, Random rand, int chunkX, int chunkZ)
    {
        if (AutomatuumConfig.OREGEN.oreGenHearanium) // Check if the ore is enabled to be generated...
        {
            for (int k = 0; k < 8; k++) // Set max spawn tries to 8
            {
                int xChunk = chunkX + rand.nextInt(16); // Calculate in which Chunk on the X-Axis the ore should be generated
                int zChunk = chunkZ + rand.nextInt(16); // Calculate in which Chunk on the Z-Axis the ore should be generated

                int getPosY = rand.nextInt(42); // Get a random Y-Axis position for the ore
                BlockPos orePos = new BlockPos(xChunk, getPosY, zChunk); // Calculate a new Block Position for the ore
                (new WorldGenMinable(BlockRegistry.HEARANIUM_ORE.getDefaultState(), 8)).generate(world, rand, orePos);

                if (AutomatuumConfig.GENERAL.oreFluidLogger) // If Ore/Fluid Generation Logging is active...
                {
                    Automatuum.LOGGER.log(Level.DEBUG, "Generated hearanium ore at chunks [" + xChunk + ", " + zChunk + "] at height Y: " + getPosY);
                }
            }
        }
    }

    private void generateSapphirite(World world, Random rand, int chunkX, int chunkZ)
    {
        if (AutomatuumConfig.OREGEN.oreGenSapphirite) // Check if the ore is enabled to be generated...
        {
            for (int k = 0; k < 9; k++) // Set max spawn tries to 9
            {
                int xChunk = chunkX + rand.nextInt(16); // Calculate in which Chunk on the X-Axis the ore should be generated
                int zChunk = chunkZ + rand.nextInt(16); // Calculate in which Chunk on the Z-Axis the ore should be generated

                int getPosY = rand.nextInt(53); // Get a random Y-Axis position for the ore
                BlockPos orePos = new BlockPos(xChunk, getPosY, zChunk); // Calculate a new Block Position for the ore
                (new WorldGenMinable(BlockRegistry.SAPPHIRITE_ORE.getDefaultState(), 10)).generate(world, rand, orePos);

                if (AutomatuumConfig.GENERAL.oreFluidLogger) // If Ore/Fluid Generation Logging is active...
                {
                    Automatuum.LOGGER.log(Level.DEBUG, "Generated sapphirite ore at chunks [" + xChunk + ", " + zChunk + "] at height Y: " + getPosY);
                }
            }
        }
    }

    private void generateCrystal(World world, Random rand, int chunkX, int chunkZ)
    {
        if (AutomatuumConfig.OREGEN.oreGenCrystal) // Check if the ore is enabled to be generated...
        {
            for (int k = 0; k < 4; k++) // Set max spawn tries to 4
            {
                int xChunk = chunkX + rand.nextInt(16); // Calculate in which Chunk on the X-Axis the ore should be generated
                int zChunk = chunkZ + rand.nextInt(16); // Calculate in which Chunk on the Z-Axis the ore should be generated

                int getPosY = rand.nextInt(12); // Get a random Y-Axis position for the ore
                BlockPos orePos = new BlockPos(xChunk, getPosY, zChunk); // Calculate a new Block Position for the ore
                (new WorldGenMinable(BlockRegistry.CRYSTAL_ORE.getDefaultState(), 5)).generate(world, rand, orePos);

                if (AutomatuumConfig.GENERAL.oreFluidLogger) // If Ore/Fluid Generation Logging is active...
                {
                    Automatuum.LOGGER.log(Level.DEBUG, "Generated crystal ore at chunks [" + xChunk + ", " + zChunk + "] at height Y: " + getPosY);
                }
            }
        }
    }

    private void generateFeros(World world, Random rand, int chunkX, int chunkZ)
    {
        if (AutomatuumConfig.OREGEN.oreGenFeros) // Check if the ore is enabled to be generated...
        {
            for (int k = 0; k < 3; k++)
            {
                int xChunk = chunkX + rand.nextInt(16); // Calculate in which Chunk on the X-Axis the ore should be generated
                int zChunk = chunkZ + rand.nextInt(16); // Calculate in which Chunk on the Z-Axis the ore should be generated

                int getPosY = rand.nextInt(95); // Get a random Y-Axis position for the ore
                BlockPos orePos = new BlockPos(xChunk, getPosY, zChunk); // Calculate a new Block Position for the ore
                (new WorldGenMinable(BlockRegistry.FEROS_ORE.getDefaultState(), 10, state -> state == Blocks.NETHERRACK.getDefaultState())).generate(world, rand, orePos);

                if (AutomatuumConfig.GENERAL.oreFluidLogger) // If Ore/Fluid Generation Logging is active...
                {
                    Automatuum.LOGGER.log(Level.DEBUG, "Generated nether ore FEROS at chunks [" + xChunk + ", " + zChunk + "] at height Y: " + getPosY);
                }
            }
        }
    }

    // Fluid Generation starts here!
    // Do not put any ores into this section EnK... you'll regret it afterwards.

    private void generateCrudeOil(World world, Random rand, int chunkX, int chunkZ)
    {
        if (AutomatuumConfig.FLUIDGEN.fluidGenCrudeOil) // Check if the fluid is enabled to be generated...
        {
            for (int k = 0; k < 2; k++) // Set max spawn tries to 2
            {
                int xChunk = chunkX + rand.nextInt(16) + 8; // Calculate in which Chunk on the X-Axis the fluid should be generated
                int zChunk = chunkZ + rand.nextInt(16) + 8; // Calculate in which Chunk on the Z-Axis the fluid should be generated
                int getPosY = rand.nextInt(32); // Get a random Y-Axis position for the fluid

                BlockPos fluidPos = new BlockPos(xChunk, getPosY, zChunk); // Calculate a new Block Position for the fluid
                new WorldGenLakes(FluidRegistry.CRUDE_OIL.getBlock()).generate(world, rand, fluidPos);

                if (AutomatuumConfig.GENERAL.oreFluidLogger) // If Ore/Fluid Generation Logging is active...
                {
                    Automatuum.LOGGER.log(Level.DEBUG, "Generated crude oil lake at chunks [" + xChunk + ", " + zChunk + "] at height Y: " + getPosY);
                }
            }
        }
    }
}
