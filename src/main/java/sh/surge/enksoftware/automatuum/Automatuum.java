package sh.surge.enksoftware.automatuum;

import sh.surge.enksoftware.automatuum.gen.GenerationHandler;
import sh.surge.enksoftware.automatuum.proxy.GuiProxy;
import net.minecraftforge.fluids.FluidRegistry;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.network.NetworkRegistry;
import net.minecraftforge.fml.common.registry.GameRegistry;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


@Mod(modid = "automatuum", name = "Automatuum", version = "1.12.2-0.4.0", updateJSON = "http://enksoftware.surge.sh/backend/updatedata/automatuum/updateData.json")
public class Automatuum
{
    @Mod.Instance
    public static Automatuum instance;

    public static final Logger LOGGER = LogManager.getLogger("automatuum");

    public static final String MOD_ID = "automatuum";

    @Mod.EventHandler
    public void preInit(FMLPreInitializationEvent event)
    {
        GameRegistry.registerWorldGenerator(new GenerationHandler(), 2);
    }

    @Mod.EventHandler
    public void init(FMLInitializationEvent event)
    {
        NetworkRegistry.INSTANCE.registerGuiHandler(instance, new GuiProxy());
    }

    @Mod.EventHandler
    public void postInit(FMLPostInitializationEvent event)
    {
    }

    static
    {
        FluidRegistry.enableUniversalBucket();
    }
}
