package sh.surge.enksoftware.automatuum.proxy;

import sh.surge.enksoftware.automatuum.client.gui.*;
import sh.surge.enksoftware.automatuum.inventory.*;
import sh.surge.enksoftware.automatuum.tileentity.ElectricBlastFurnaceTileEntity;
import sh.surge.enksoftware.automatuum.tileentity.OilRefinerTileEntity;
import sh.surge.enksoftware.automatuum.tileentity.UniversalGeneratorTileEntity;
import sh.surge.enksoftware.automatuum.tileentity.CrystalBlockTileEntity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.network.IGuiHandler;

import javax.annotation.Nullable;

public class GuiProxy implements IGuiHandler
{
    public static final int CRYSTAL = 0;
    public static final int HIGH_FURNACE = 1;
    public static final int CRUDE_POWER_GENERATOR = 2;
    public static final int REFINED_POWER_GENERATOR = 3;
    public static final int OIL_REFINER = 4;

    @Override
    @Nullable
    public Object getServerGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z)
    {
        BlockPos pos = new BlockPos(x, y, z);
        if (world.isBlockLoaded(pos))
        {
            TileEntity tileEntity = world.getTileEntity(pos);
            switch (ID)
            {
                case CRYSTAL:
                    if (tileEntity instanceof CrystalBlockTileEntity)
                    {
                        CrystalBlockTileEntity tileEntity1 = (CrystalBlockTileEntity) tileEntity;
                        if (tileEntity1.canInteractWith(player))
                        {
                            return new CrystalContainer(player, tileEntity1);
                        }
                    }
                    break;
                case CRUDE_POWER_GENERATOR:
                    if (tileEntity instanceof UniversalGeneratorTileEntity.CrudeOilGeneratorTE)
                    {
                        UniversalGeneratorTileEntity.CrudeOilGeneratorTE tileEntity1 = (UniversalGeneratorTileEntity.CrudeOilGeneratorTE) tileEntity;
                        if (tileEntity1.canInteractWith(player))
                        {
                            return new CrudeGenContainer(player, tileEntity1);
                        }
                    }
                case REFINED_POWER_GENERATOR:
                    if (tileEntity instanceof UniversalGeneratorTileEntity.RefinedOilGeneratorTE)
                    {
                        UniversalGeneratorTileEntity.RefinedOilGeneratorTE tileEntity1 = (UniversalGeneratorTileEntity.RefinedOilGeneratorTE) tileEntity;
                        if (tileEntity1.canInteractWith(player))
                        {
                            return new RefinedGenContainer(player, tileEntity1);
                        }
                    }
                case HIGH_FURNACE:
                    if (tileEntity instanceof ElectricBlastFurnaceTileEntity)
                    {
                        ElectricBlastFurnaceTileEntity tileEntity1 = (ElectricBlastFurnaceTileEntity) tileEntity;
                        if (tileEntity1.canInteractWith(player))
                        {
                            return new ElectricalBlastFurnaceContainer(player, tileEntity1);
                        }
                    }
                case OIL_REFINER:
                    if (tileEntity instanceof OilRefinerTileEntity)
                    {
                        OilRefinerTileEntity tileEntity1 = (OilRefinerTileEntity) tileEntity;
                        if (tileEntity1.canInteractWith(player))
                        {
                            return new OilRefinerContainer(player, tileEntity1);
                        }
                    }
                default:
                    break;
            }
        }
        return null;
    }

    @Override
    @Nullable
    public Object getClientGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z)
    {
        BlockPos pos = new BlockPos(x, y, z);
        if (world.isBlockLoaded(pos))
        {
            TileEntity tileEntity = world.getTileEntity(pos);
            switch (ID)
            {
                case CRYSTAL:
                    if (tileEntity instanceof CrystalBlockTileEntity)
                    {
                        CrystalBlockTileEntity tileEntity1 = (CrystalBlockTileEntity) tileEntity;
                        if (tileEntity1.canInteractWith(player))
                        {
                            return new CBlockGuiContainer(player, tileEntity1);
                        }
                    }
                    break;
                case CRUDE_POWER_GENERATOR:
                    if (tileEntity instanceof UniversalGeneratorTileEntity.CrudeOilGeneratorTE)
                    {
                        UniversalGeneratorTileEntity.CrudeOilGeneratorTE tileEntity1 = (UniversalGeneratorTileEntity.CrudeOilGeneratorTE) tileEntity;
                        if (tileEntity1.canInteractWith(player))
                        {
                            return new CrudeGenGuiContainer(player, tileEntity1);
                        }
                    }
                case REFINED_POWER_GENERATOR:
                    if (tileEntity instanceof UniversalGeneratorTileEntity.RefinedOilGeneratorTE)
                    {
                        UniversalGeneratorTileEntity.RefinedOilGeneratorTE tileEntity1 = (UniversalGeneratorTileEntity.RefinedOilGeneratorTE) tileEntity;
                        if (tileEntity1.canInteractWith(player))
                        {
                            return new RefinedGenGuiContainer(player, tileEntity1);
                        }
                    }
                case HIGH_FURNACE:
                    if (tileEntity instanceof ElectricBlastFurnaceTileEntity)
                    {
                        ElectricBlastFurnaceTileEntity tileEntity1 = (ElectricBlastFurnaceTileEntity) tileEntity;
                        if (tileEntity1.canInteractWith(player))
                        {
                            return new ElectricalBlastFurnaceGuiContainer(player, tileEntity1);
                        }
                    }
                case OIL_REFINER:
                    if (tileEntity instanceof OilRefinerTileEntity)
                    {
                        OilRefinerTileEntity tileEntity1 = (OilRefinerTileEntity) tileEntity;
                        if (tileEntity1.canInteractWith(player))
                        {
                            return new OilRefinerGuiContainer(player, tileEntity1);
                        }
                    }
                default:
                    break;
            }
        }
        return null;
    }
}
