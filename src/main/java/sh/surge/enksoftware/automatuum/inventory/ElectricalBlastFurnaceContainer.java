package sh.surge.enksoftware.automatuum.inventory;

import sh.surge.enksoftware.automatuum.tileentity.ElectricBlastFurnaceTileEntity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.SlotItemHandler;

import javax.annotation.Nonnull;

public class ElectricalBlastFurnaceContainer extends Container
{
    // General offsets and declarations
    public static final int xOffset = 8;
    public static final int yOffset = 84;
    public static final int tileYOffset = 41;
    public static final int slotBorder = 1;
    public static final int invSlotsVert = 3;
    public static final int invSlotsHorz = 9;
    public static final int hotbarSlotsHorz = 9;
    public static final int hotbarSeparation = 4;

    // Input Slots
    public static final int tileInvSlot0 = 44;
    public static final int tileInvSlot1 = 26;
    public static final int tileInvSlot2 = 44;
    public static final int tileInvSlot3 = 62;
    public static final int tileInvSlot4 = 44;

    // Input Slots y-offsets
    public static final int tileInvSlot0YOffset = 24;
    public static final int tileInvSlot1YOffset = 6;
    public static final int tileInvSlot2YOffset = 6;
    public static final int tileInvSlot3YOffset = 6;
    public static final int tileInvSlot4YOffset = 12;

    // Output slot
    public static final int tileInvSlotOut = 127;

    // Output slot y-offset
    public static final int tileInvSlotOutYOffset = 7;

    // TileEntity
    public ElectricBlastFurnaceTileEntity blockTileEntity;

    // Ctr
    public ElectricalBlastFurnaceContainer(EntityPlayer player, ElectricBlastFurnaceTileEntity tileEntity)
    {
        this.blockTileEntity = tileEntity;
        addPlayerInventory(player);

        if (blockTileEntity.hasCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY, null))
        {
            addTileEntityInventory();
        }
    }

    // Add the player inventory to the container
    public void addPlayerInventory(EntityPlayer playerIn)
    {
        IInventory playerInv = playerIn.inventory;
        for (int row = 0; row < invSlotsVert; row++)
        {
            for (int col = 0; col < invSlotsHorz; col++)
            {
                int x = xOffset + col * (16 + (slotBorder * 2));
                int y = row * (16 + (slotBorder * 2)) + yOffset;

                this.addSlotToContainer(new Slot(playerInv, col + row * invSlotsHorz + hotbarSlotsHorz, x, y));
            }
        }

        int hotbarY = (invSlotsVert * (16 + (slotBorder * 2))) + hotbarSeparation + yOffset;

        for (int col = 0; col < invSlotsHorz; col++)
        {
            int x = xOffset + col * (16 + (slotBorder * 2));
            this.addSlotToContainer(new Slot(playerInv, col, x, hotbarY));
        }
    }

    // Add the TileEntity inventory to the container
    public void addTileEntityInventory()
    {
        IItemHandler inv  = blockTileEntity.getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY, null);

        // Add the Input Slots
        addSlotToContainer(new SlotItemHandler(inv, 0, tileInvSlot0, tileYOffset - tileInvSlot0YOffset));
        addSlotToContainer(new SlotItemHandler(inv, 1, tileInvSlot1, tileYOffset - tileInvSlot1YOffset));
        addSlotToContainer(new SlotItemHandler(inv, 2, tileInvSlot2, tileYOffset - tileInvSlot2YOffset));
        addSlotToContainer(new SlotItemHandler(inv, 3, tileInvSlot3, tileYOffset - tileInvSlot3YOffset));
        addSlotToContainer(new SlotItemHandler(inv, 4, tileInvSlot4, tileYOffset + tileInvSlot4YOffset));

        // Add the output slot and make it unusable to insert an item there
        addSlotToContainer(new SlotItemHandler(inv, 5, tileInvSlotOut, tileYOffset - tileInvSlotOutYOffset)
        {
            @Override
            public boolean isItemValid(@Nonnull ItemStack stack)
            {
                return false;
            }
        });
    }

    // Can the player interact with the container?
    @Override
    public boolean canInteractWith(EntityPlayer playerIn)
    {
        return blockTileEntity.canInteractWith(playerIn);
    }

    // Transfer Items into slot... (Not working, but crash circumvention)
    @Override
    public ItemStack transferStackInSlot(EntityPlayer playerIn, int index)
    {
        ItemStack itemStack = ItemStack.EMPTY;
        Slot slot = inventorySlots.get(index);

        if (slot != null && slot.getHasStack())
        {
            ItemStack itemStack1 = slot.getStack();
            itemStack = itemStack1.copy();

            int containerSlots = inventorySlots.size() - playerIn.inventory.mainInventory.size();

            if (index < containerSlots)
            {
                if (!this.mergeItemStack(itemStack1, containerSlots, inventorySlots.size(), false))
                {
                    return ItemStack.EMPTY;
                }
            }
            else if (!this.mergeItemStack(itemStack1, 0, containerSlots, false))
            {
                return ItemStack.EMPTY;
            }

            if (itemStack1.isEmpty())
            {
                slot.putStack(ItemStack.EMPTY);
            }
            else
            {
                slot.onSlotChanged();
            }

            if (itemStack1.getCount() == itemStack.getCount())
            {
                return ItemStack.EMPTY;
            }

            slot.onTake(playerIn, itemStack1);
        }

        return itemStack;
    }
}
