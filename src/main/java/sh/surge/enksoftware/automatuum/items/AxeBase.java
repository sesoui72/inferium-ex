package sh.surge.enksoftware.automatuum.items;

import sh.surge.enksoftware.automatuum.AutomatuumTab;
import net.minecraft.item.Item;
import net.minecraft.item.ItemAxe;

public class AxeBase extends ItemAxe
{
    public AxeBase(String name, Item.ToolMaterial material, float damage, float speed)
    {
        super(material, damage, speed);
        setRegistryName(name);
        setTranslationKey("automatuum." + name);
        setHarvestLevel("axe", material.getHarvestLevel());
        setCreativeTab(AutomatuumTab.AUTOMATUUM_TAB);
    }
}
