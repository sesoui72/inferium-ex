package sh.surge.enksoftware.automatuum.items;

import sh.surge.enksoftware.automatuum.AutomatuumTab;
import net.minecraft.item.Item;
import net.minecraft.item.ItemPickaxe;

public class PickaxeBase extends ItemPickaxe
{
    public PickaxeBase(String name, Item.ToolMaterial material)
    {
        super(material);
        setRegistryName(name);
        setTranslationKey("automatuum." + name);
        setHarvestLevel("pickaxe", material.getHarvestLevel());
        setCreativeTab(AutomatuumTab.AUTOMATUUM_TAB);
    }
}
