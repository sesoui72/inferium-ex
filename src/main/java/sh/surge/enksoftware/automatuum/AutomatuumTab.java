package sh.surge.enksoftware.automatuum;

import sh.surge.enksoftware.automatuum.blocks.BlockRegistry;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class AutomatuumTab
{
    public static final CreativeTabs AUTOMATUUM_TAB = new CreativeTabs("automatuum")
    {
        @Override
        @SideOnly(Side.CLIENT)
        public ItemStack createIcon()
        {
            return new ItemStack(BlockRegistry.LARANIUM_ORE);
        }
    };
}
