package sh.surge.enksoftware.automatuum.recipes;

import sh.surge.enksoftware.automatuum.Automatuum;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.registries.IForgeRegistry;
import net.minecraftforge.registries.RegistryBuilder;
import sh.surge.enksoftware.automatuum.items.ItemRegistry;

import javax.annotation.Nullable;

import static net.minecraft.init.Blocks.IRON_BLOCK;

@Mod.EventBusSubscriber(modid = "automatuum")
public class ElectricalBlastFurnaceRecipeManager
{
    public static IForgeRegistry<ElectricalBlastFurnaceRecipe> REGISTRY;

    @SubscribeEvent
    public static void registerTaskRegistry(RegistryEvent.NewRegistry event)
    {
        REGISTRY = new RegistryBuilder<ElectricalBlastFurnaceRecipe>()
                .setName(new ResourceLocation(Automatuum.MOD_ID, "electricalblastfurnacerecipes"))
                .setType(ElectricalBlastFurnaceRecipe.class)
                .allowModification()
                .disableSaving()
                .create();
    }

    @SubscribeEvent
    public static void registerRecipes(RegistryEvent.Register<ElectricalBlastFurnaceRecipe> event)
    {
        event.getRegistry().registerAll(
                new ElectricalBlastFurnaceRecipe(new ItemStack(IRON_BLOCK),
                        new ItemStack(ItemRegistry.CARBON),
                        new ItemStack(ItemRegistry.CARBON),
                        new ItemStack(ItemRegistry.CARBON),
                        new ItemStack(IRON_BLOCK),
                        new ItemStack(ItemRegistry.STEEL_PLATE),
                        100,
                        25,
                        0.75F
                )
                .setRegistryName("steel_plate")
        );
    }

    @Nullable
    public static ElectricalBlastFurnaceRecipe getRecipe(IItemHandler inventory)
    {
        return inventory.getSlots() < 5 ? null :
                getRecipe(inventory.getStackInSlot(0), inventory.getStackInSlot(1), inventory.getStackInSlot(2), inventory.getStackInSlot(3), inventory.getStackInSlot(4));
    }

    @Nullable
    public static ElectricalBlastFurnaceRecipe getRecipe(ItemStack input1, ItemStack input2, ItemStack input3, ItemStack input4, ItemStack input5)
    {
        for (ElectricalBlastFurnaceRecipe recipe : REGISTRY.getValuesCollection())
        {
            if (recipe.in1.apply(input1) &&
                    recipe.in2.apply(input2) &&
                    recipe.in3.apply(input3) &&
                    recipe.in4.apply(input4) &&
                    recipe.in5.apply(input5)
                    )
            {
                return recipe;
            }
        }
        return null;
    }
}
