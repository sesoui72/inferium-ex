package sh.surge.enksoftware.automatuum.recipes;

import sh.surge.enksoftware.automatuum.Automatuum;
import sh.surge.enksoftware.automatuum.fluid.FluidRegistry;
import sh.surge.enksoftware.automatuum.items.ItemRegistry;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fluids.Fluid;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.FluidUtil;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.registries.IForgeRegistry;
import net.minecraftforge.registries.RegistryBuilder;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.HashMap;
import java.util.Map;

@Mod.EventBusSubscriber(modid = "automatuum")
public class OilRefinerRecipeManager
{
    public static IForgeRegistry<OilRefinerRecipe> REGISTRY;
    private static final Map<ItemStack, Float> refiningChanceList = new HashMap<ItemStack, Float>();
    private static final Map<ItemStack, Float> refiningSpeedList = new HashMap<ItemStack, Float>();

    @SubscribeEvent
    public static void registerRecipes(RegistryEvent.Register<OilRefinerRecipe> event)
    {
        // Refining Chances
        refiningChanceList.put(new ItemStack(ItemRegistry.HARSH_REFINING_SIEVE), 0.3F);
        refiningChanceList.put(new ItemStack(ItemRegistry.FINE_REFINING_SIEVE), 0.5F);
        refiningChanceList.put(new ItemStack(ItemRegistry.FINER_REFINING_SIEVE), 0.7F);
        refiningChanceList.put(new ItemStack(ItemRegistry.FINEST_REFINING_SIEVE), 1.0F);

        // Refining Time Increasings.
        refiningSpeedList.put(new ItemStack(ItemRegistry.HARSH_REFINING_SIEVE), 1.0F);
        refiningSpeedList.put(new ItemStack(ItemRegistry.FINE_REFINING_SIEVE), 1.15F);
        refiningSpeedList.put(new ItemStack(ItemRegistry.FINER_REFINING_SIEVE), 1.2F);
        refiningSpeedList.put(new ItemStack(ItemRegistry.FINEST_REFINING_SIEVE), 1.305F);


        event.getRegistry().registerAll(
                new OilRefinerRecipe(
                        new FluidStack(FluidRegistry.CRUDE_OIL, Fluid.BUCKET_VOLUME),
                        new FluidStack(FluidRegistry.REFINED_OIL, Fluid.BUCKET_VOLUME),
                        600,
                        50
                ).setRegistryName("oil_crude_to_oil_refined")
        );
    }

    @Nullable
    public static OilRefinerRecipe getRecipe(@Nonnull ItemStack stack)
    {
        return getRecipe(FluidUtil.getFluidContained(stack));
    }

    @Nullable
    public static OilRefinerRecipe getRecipe(@Nonnull FluidStack stack)
    {
        for (OilRefinerRecipe recipe : REGISTRY.getValuesCollection())
        {
            if (recipe.input.isFluidEqual(stack) && stack.amount >= recipe.input.amount)
            {
                return recipe;
            }
        }

        return null;
    }

    @SubscribeEvent
    public static void registerTaskRegistry(RegistryEvent.NewRegistry event)
    {
        REGISTRY = new RegistryBuilder<OilRefinerRecipe>()
                .setName(new ResourceLocation(Automatuum.MOD_ID, "oilrefinerrecipes"))
                .setType(OilRefinerRecipe.class)
                .allowModification()
                .disableSaving()
                .create();
    }
}
