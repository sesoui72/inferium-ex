package sh.surge.enksoftware.automatuum.util;

import net.minecraftforge.energy.EnergyStorage;

public class MachineEnergyStorage extends EnergyStorage
{
    public MachineEnergyStorage(int capacity)
    {
        super(capacity, capacity, capacity, 0);
    }

    public MachineEnergyStorage(int capacity, int maxTransfer)
    {
        super(capacity, maxTransfer, maxTransfer, 0);
    }

    public MachineEnergyStorage(int capacity, int maxReceive, int maxExtract)
    {
        super(capacity, maxReceive, maxExtract, 0);
    }

    public MachineEnergyStorage(int capacity, int maxReceive, int maxExtract, int energy)
    {
        super(capacity, maxReceive, maxExtract, energy);
    }

    public void setEnergy(int amount)
    {
        energy = amount;
    }
}