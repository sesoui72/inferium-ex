package sh.surge.enksoftware.automatuum.util;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.texture.TextureAtlasSprite;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.fluids.FluidStack;

public class RenderUtil
{
    public static void renderFluidCuboid(BufferBuilder renderer, FluidStack fluid, BlockPos pos, double x1, double y1, double z1, double x2, double y2, double z2)
    {
        Minecraft mc = Minecraft.getMinecraft();
        TextureAtlasSprite still = mc.getTextureMapBlocks().getTextureExtry(fluid.getFluid().getStill(fluid).toString());
        TextureAtlasSprite flowing = mc.getTextureMapBlocks().getTextureExtry(fluid.getFluid().getFlowing(fluid).toString());

        int brightness = mc.world.getCombinedLight(pos, fluid.getFluid().getLuminosity(fluid));
        int color = fluid.getFluid().getColor(fluid);
        boolean gaseous = fluid.getFluid().isGaseous(fluid);

        putTexturedQuad(renderer, still,   x1, y1, z1, x2 - x1, y2 - y1, z2 - z2, EnumFacing.DOWN, color, brightness, false, gaseous);
        putTexturedQuad(renderer, flowing, x1, y1, z1, x2 - x1, y2 - y1, z2 - z1, EnumFacing.NORTH, color, brightness, true, gaseous);
        putTexturedQuad(renderer, flowing, x1, y1, z1, x2 - x1, y2 - y1, z2 - z1, EnumFacing.EAST, color, brightness, true, gaseous);
        putTexturedQuad(renderer, flowing, x1, y1, z1, x2 - x1, y2 - y1, z2 - z1, EnumFacing.SOUTH, color, brightness, true, gaseous);
        putTexturedQuad(renderer, flowing, x1, y1, z1, x2 - x1, y2 - y1, z2 - z1, EnumFacing.WEST, color, brightness, true, gaseous);
        putTexturedQuad(renderer, still,   x1, y1, z1, x2 - x1, y2 - y1, z2 - z1, EnumFacing.UP, color, brightness, false, gaseous);
    }

    public static void putTexturedQuad(BufferBuilder renderer, TextureAtlasSprite sprite, double x, double y, double z, double w, double h, double d, EnumFacing face, int color, int brightness, boolean flowing, boolean flipVertically)
    {
        // int skyLight = brightness >> 0x16 & 0xFFFF;
        // int skyLight = brightness >> 16 & 0xFFFF;
        int skyLight = brightness >> 0x10 & 0xFFFF;
        int blockLight = brightness & 0xFFFF;

        int a = color >> 24 & 0xFF;
        int r = color >> 16 & 0xFF;
        int g = color >> 8 & 0xFF;
        int b = color & 0xFF;

        putTexturedQuad(renderer, sprite, x, y, z, w, h, d, face, r, g, b, a, skyLight, blockLight, flowing, flipVertically);
    }

    public static void putTexturedQuad(BufferBuilder renderer, TextureAtlasSprite sprite, double x, double y, double z, double w, double h, double d, EnumFacing face, int r, int g, int b, int a, int skyLight, int blockLight, boolean flowing, boolean flipVertically)
    {
        if (sprite == null)
        {
            return;
        }

        double minU;
        double maxU;
        double minV;
        double maxV;

        double x1 = x;
        double y1 = y;
        double z1 = z;
        double x2 = x + w;
        double y2 = y + h;
        double z2 = z + d;

        {
            double texX1 = x1 % 1.0D;
            double texX2 = texX1 + w;
            while (texX2 > 1.0F)
                texX2 -= 1.0F;

            double texY1 = y1 % 1.0D;
            double texY2 = texY1 + h;
            while (texY2 > 1.0F)
                texY2 -= 1.0F;

            double texZ1 = z1 % 1.0D;
            double texZ2 = texZ1 + d;
            while (texZ2 > 1.0F)
                texZ2 -= 1.0F;

            double size = 16.0F;
            if (flowing)
            {
                size = 8.0F;

                double tmp = 1.0D - texY1;
                texY1 = 1.0D - texY2;
                texY2 = tmp;
            }

            switch (face)
            {
                case UP:
                case DOWN:
                    minU = sprite.getInterpolatedU(texX1 * size);
                    maxU = sprite.getInterpolatedU(texX2 * size);
                    minV = sprite.getInterpolatedV(texZ1 * size);
                    maxV = sprite.getInterpolatedV(texZ2 * size);
                    break;
                case NORTH:
                case SOUTH:
                    minU = sprite.getInterpolatedU(texX2 * size);
                    maxU = sprite.getInterpolatedU(texX1 * size);
                    minV = sprite.getInterpolatedV(texY1 * size);
                    maxV = sprite.getInterpolatedV(texY2 * size);
                    break;
                case EAST:
                case WEST:
                    minU = sprite.getInterpolatedU(texZ2 * size);
                    maxU = sprite.getInterpolatedU(texZ1 * size);
                    minV = sprite.getInterpolatedV(texY1 * size);
                    maxV = sprite.getInterpolatedV(texY2 * size);
                    break;
                default:
                    minU = sprite.getMinU();
                    maxU = sprite.getMaxU();
                    minV = sprite.getMinV();
                    maxV = sprite.getMaxV();
            }

            if (flipVertically)
            {
                double tmp = minV;
                minV = maxV;
                maxV = tmp;
            }
        }

        switch (face)
        {
            case DOWN:
                addVertex(renderer, x1, y1, z1, r, g, b, a, minU, minV, skyLight, blockLight);
                addVertex(renderer, x2, y1, z1, r, g, b, a, maxU, minV, skyLight, blockLight);
                addVertex(renderer, x2, y1, z2, r, g, b, a, maxU, maxV, skyLight, blockLight);
                addVertex(renderer, x1, y1, z2, r, g, b, a, minU, maxV, skyLight, blockLight);
                break;
            case UP:
                addVertex(renderer, x1, y2, z1, r, g, b, a, minU, minV, skyLight, blockLight);
                addVertex(renderer, x1, y2, z2, r, g, b, a, minU, maxV, skyLight, blockLight);
                addVertex(renderer, x2, y2, z2, r, g, b, a, maxU, maxV, skyLight, blockLight);
                addVertex(renderer, x2, y2, z1, r, g, b, a, maxU, minV, skyLight, blockLight);
                break;
            case NORTH:
                addVertex(renderer, x1, y1, z1, r, g, b, a, minU, maxV, skyLight, blockLight);
                addVertex(renderer, x1, y2, z1, r, g, b, a, minU, minV, skyLight, blockLight);
                addVertex(renderer, x2, y2, z1, r, g, b, a, maxU, minV, skyLight, blockLight);
                addVertex(renderer, x2, y1, z1, r, g, b, a, maxU, maxV, skyLight, blockLight);
                break;
            case SOUTH:
                addVertex(renderer, x1, y1, z2, r, g, b, a, maxU, maxV, skyLight, blockLight);
                addVertex(renderer, x2, y1, z2, r, g, b, a, minU, maxV, skyLight, blockLight);
                addVertex(renderer, x2, y2, z2, r, g, b, a, minU, minV, skyLight, blockLight);
                addVertex(renderer, x1, y2, z2, r, g, b, a, maxU, minV, skyLight, blockLight);
                break;
            case WEST:
                addVertex(renderer, x1, y1, z1, r, g, b, a, maxU, maxV, skyLight, blockLight);
                addVertex(renderer, x1, y1, z2, r, g, b, a, minU, maxV, skyLight, blockLight);
                addVertex(renderer, x1, y2, z2, r, g, b, a, minU, minV, skyLight, blockLight);
                addVertex(renderer, x1, y2, z1, r, g, b, a, maxU, minV, skyLight, blockLight);
                break;
            case EAST:
                addVertex(renderer, x2, y1, z1, r, g, b, a, minU, maxV, skyLight, blockLight);
                addVertex(renderer, x2, y2, z1, r, g, b, a, minU, minV, skyLight, blockLight);
                addVertex(renderer, x2, y2, z2, r, g, b, a, maxU, minV, skyLight, blockLight);
                addVertex(renderer, x2, y1, z2, r, g, b, a, maxU, maxV, skyLight, blockLight);
                break;
        }
    }

    private static void addVertex(BufferBuilder renderer, double x, double y, double z, int r, int g, int b, int a, double u, double v, int skyLight, int blockLight)
    {
        renderer.pos(x, y, z).color(r, g, b, a).tex(u, v).lightmap(skyLight, blockLight).endVertex();
    }
}
