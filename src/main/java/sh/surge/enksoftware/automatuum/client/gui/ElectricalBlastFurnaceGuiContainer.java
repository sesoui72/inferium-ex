package sh.surge.enksoftware.automatuum.client.gui;

import sh.surge.enksoftware.automatuum.Automatuum;
import sh.surge.enksoftware.automatuum.inventory.ElectricalBlastFurnaceContainer;
import sh.surge.enksoftware.automatuum.tileentity.ElectricBlastFurnaceTileEntity;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.util.ResourceLocation;

public class ElectricalBlastFurnaceGuiContainer extends GuiContainer
{
    // Texture declaration
    private static final ResourceLocation CONTAINER_TEXTURE = new ResourceLocation(Automatuum.MOD_ID, "textures/gui/container/high_furnace.png");

    // Fields
    private IInventory playerInventory;
    private ElectricBlastFurnaceTileEntity blockTileEntity;

    // Constructor
    public ElectricalBlastFurnaceGuiContainer(EntityPlayer player, ElectricBlastFurnaceTileEntity tileEntity)
    {
        super(new ElectricalBlastFurnaceContainer(player, tileEntity));
        this.playerInventory = player.inventory;
        this.blockTileEntity = tileEntity;
    }

    @Override
    public void drawScreen(int mouseX, int mouseY, float partialTicks)
    {
        this.drawDefaultBackground();
        super.drawScreen(mouseX, mouseY, partialTicks);
        this.renderHoveredToolTip(mouseX, mouseY);
    }

    @Override
    protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY)
    {
        GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
        this.mc.getTextureManager().bindTexture(CONTAINER_TEXTURE);
        int i = (this.width - this.xSize) / 2;
        int j = (this.height - this.ySize) / 2;
        this.drawTexturedModalRect(i, j, 0, 0, this.xSize, this.ySize);

        int k = this.getProgressLevelScaled(24);
        this.drawTexturedModalRect(i + 89, j + 35, 176, 0, k, 16);
    }

    @Override
    protected void drawGuiContainerForegroundLayer(int mouseX, int mouseY)
    {
        String s = "Electrical Blast Furnace";
        this.fontRenderer.drawString(s, this.xSize / 2 - this.fontRenderer.getStringWidth(s) / 2, 6, 0x404040);
        this.fontRenderer.drawString(this.playerInventory.getDisplayName().getUnformattedText(), 8, this.ySize - 96 + 2, 0x404040);

    }

    private int getProgressLevelScaled(int pixels)
    {
        final int i = this.blockTileEntity.processTime;
        final int j = this.blockTileEntity.totalProcessTime;

        return j != 0 && i != 0 ? i * pixels / j : 0;
    }
}
