package sh.surge.enksoftware.automatuum.tileentity;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SPacketUpdateTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ITickable;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.energy.CapabilityEnergy;
import net.minecraftforge.energy.EnergyStorage;
import net.minecraftforge.energy.IEnergyStorage;

import javax.annotation.Nullable;

public class UniversalCableTileEntity extends TileEntity implements ITickable
{
    // Subclasses
    public static class TopariteCableTileEntity extends UniversalCableTileEntity
    {
        public TopariteCableTileEntity()
        {
            super(2000, 50);
        }
    }
    // Fields
    IEnergyStorage energyStorage;

    // Constructor
    public UniversalCableTileEntity(int energyCapacity, int energyTransfer)
    {
        this.energyStorage = new EnergyStorage(energyCapacity, energyTransfer);
    }

    @Override
    public void update()
    {
        if (getWorld() == null || getWorld().isRemote)
        {
            return;
        }

        boolean hasChanged = false;
        IEnergyStorage energyStorage = getCapability(CapabilityEnergy.ENERGY, null);
        Capability<? extends IEnergyStorage> energyCapability = CapabilityEnergy.ENERGY;

        for (EnumFacing facing : EnumFacing.VALUES)
        {
            BlockPos adjacent = getPos().offset(facing);
            if (getWorld().isBlockLoaded(adjacent))
            {
                TileEntity te = getWorld().getTileEntity(getPos().offset(facing));
                if (te != null && te.hasCapability(energyCapability, facing.getOpposite()))
                {
                    IEnergyStorage dest = te.getCapability(energyCapability, facing.getOpposite());
                    int transEnergy;

                    if (te instanceof UniversalCableTileEntity && dest.getEnergyStored() >= energyStorage.getEnergyStored())
                    {
                        transEnergy = dest.extractEnergy(energyStorage.receiveEnergy(dest.extractEnergy(Integer.MAX_VALUE, true),  false), false);
                    }
                    else
                    {
                        transEnergy = dest.receiveEnergy(energyStorage.extractEnergy(dest.receiveEnergy(Integer.MAX_VALUE, true), false), false);
                    }

                    if (transEnergy > 0)
                    {
                        hasChanged = true;
                    }
                }
            }
        }

        if (hasChanged)
        {
            markDirty();
        }
    }

    @Nullable
    @Override
    public SPacketUpdateTileEntity getUpdatePacket()
    {
        return new SPacketUpdateTileEntity(getPos(), 0, getUpdateTag());
    }

    @Override
    public void onDataPacket(NetworkManager net, SPacketUpdateTileEntity pkt)
    {
        handleUpdateTag(pkt.getNbtCompound());
    }

    @Override
    public void readFromNBT(NBTTagCompound compound)
    {
        super.readFromNBT(compound);

        if (compound.hasKey("energy"))
        {
            CapabilityEnergy.ENERGY.readNBT(energyStorage, null, compound.getTag("energy"));
        }
    }

    @Override
    public NBTTagCompound writeToNBT(NBTTagCompound compound)
    {
        super.writeToNBT(compound);
        compound.setTag("energy", CapabilityEnergy.ENERGY.writeNBT(energyStorage, null));
        return compound;
    }

    @Override
    public boolean hasCapability(Capability<?> capability, @Nullable EnumFacing facing)
    {
        return capability == CapabilityEnergy.ENERGY ||
                super.hasCapability(capability, facing);
    }

    @Nullable
    @Override
    public <T> T getCapability(Capability<T> capability, @Nullable EnumFacing facing)
    {
        if (capability == CapabilityEnergy.ENERGY)
        {
            return CapabilityEnergy.ENERGY.cast(energyStorage);
        }
        return super.getCapability(capability, facing);
    }
}
