package sh.surge.enksoftware.automatuum.tileentity;

import sh.surge.enksoftware.automatuum.recipes.ElectricalBlastFurnaceRecipe;
import sh.surge.enksoftware.automatuum.recipes.ElectricalBlastFurnaceRecipeManager;

import sh.surge.enksoftware.automatuum.util.MachineEnergyStorage;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.item.EntityXPOrb;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SPacketUpdateTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ITickable;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.energy.CapabilityEnergy;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.IItemHandlerModifiable;
import net.minecraftforge.items.ItemStackHandler;
import net.minecraftforge.items.wrapper.RangedWrapper;

import javax.annotation.Nullable;
import java.util.Optional;

public class ElectricBlastFurnaceTileEntity extends TileEntity implements ITickable
{
    // Fields
    IItemHandlerModifiable itemHandler = new ItemStackHandler(6)
    {
        @Override
        protected void onContentsChanged(int slot)
        {
            ElectricBlastFurnaceTileEntity.this.markDirty();

            Optional.ofNullable(getWorld()).ifPresent(world ->
            {
                IBlockState state = world.getBlockState(getPos());
                world.notifyBlockUpdate(getPos(), state, state, 3);
            });
        }
    };

    MachineEnergyStorage energyStorage = new MachineEnergyStorage(5000, 500, 0);
    IItemHandlerModifiable inputSlots = new RangedWrapper(itemHandler, 0, 5);
    IItemHandlerModifiable outputSlot = new RangedWrapper(itemHandler, 5, 6);
    public int processTime = 0;
    public int totalProcessTime;

    @Nullable
    @Override
    public SPacketUpdateTileEntity getUpdatePacket()
    {
        return new SPacketUpdateTileEntity(getPos(), 0, getUpdateTag());
    }

    @Override
    public void onDataPacket(NetworkManager net, SPacketUpdateTileEntity pkt)
    {
        handleUpdateTag(pkt.getNbtCompound());
    }

    @Override
    public NBTTagCompound getUpdateTag()
    {
        NBTTagCompound tag = super.getUpdateTag();
        tag.setTag("energy", CapabilityEnergy.ENERGY.writeNBT(energyStorage, null));
        tag.setFloat("processTime", processTime);
        tag.setFloat("totalProcessTime", totalProcessTime);
        return tag;
    }

    @Override
    public void readFromNBT(NBTTagCompound compound)
    {
        super.readFromNBT(compound);
        if (compound.hasKey("inventory"))
        {
            CapabilityItemHandler.ITEM_HANDLER_CAPABILITY.readNBT(itemHandler, null, compound.getTag("inventory"));
        }
        CapabilityEnergy.ENERGY.readNBT(energyStorage, null, compound.getTag("energy"));
        processTime = compound.getInteger("processTime");
        totalProcessTime = compound.getInteger("totalProcessTime");
    }

    @Override
    public NBTTagCompound writeToNBT(NBTTagCompound compound)
    {
        compound = super.writeToNBT(compound);
        compound.setTag("inventory", CapabilityItemHandler.ITEM_HANDLER_CAPABILITY.writeNBT(itemHandler, null));
        compound.setTag("energy", CapabilityEnergy.ENERGY.writeNBT(energyStorage, null));
        compound.setInteger("processTime", processTime);
        compound.setInteger("totalProcessTime", totalProcessTime);
        return compound;
    }

    @Override
    public boolean hasCapability(Capability<?> capability, @Nullable EnumFacing facing)
    {
        return capability == CapabilityEnergy.ENERGY ||
                capability == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY ||
                super.hasCapability(capability, facing);
    }

    @Nullable
    @Override
    public <T> T getCapability(Capability<T> capability, @Nullable EnumFacing facing)
    {
        if (capability == CapabilityEnergy.ENERGY)
        {
            return CapabilityEnergy.ENERGY.cast(energyStorage);
        }
        else if (capability == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY)
        {
            return CapabilityItemHandler.ITEM_HANDLER_CAPABILITY.cast(
                    facing == null ? itemHandler : facing == EnumFacing.DOWN ? outputSlot : inputSlots
            );
        }
        return super.getCapability(capability, facing);
    }

    public boolean canInteractWith(EntityPlayer player)
    {
        return !isInvalid() && player.getDistanceSq(pos.add(0.5D, 0.5D, 0.5D)) <= 64D;
    }

    @Override
    public void update()
    {
        if (getWorld() == null || getWorld().isRemote)
        {
            return;
        }

        boolean changed = false;
        boolean notify = false;

        ElectricalBlastFurnaceRecipe recipe = ElectricalBlastFurnaceRecipeManager.getRecipe(inputSlots);

        if (recipe != null)
        {
            if (canProcess(recipe.out) && energyStorage.getEnergyStored() >= recipe.energyPerTick)
            {
                this.processTime++;
                this.totalProcessTime = recipe.ticks;
                energyStorage.setEnergy(energyStorage.getEnergyStored() - recipe.energyPerTick);

                if (this.processTime >= recipe.ticks)
                {
                    this.processTime = 0;
                    this.processItem(recipe);
                }
                notify = true;
            }
            else if (this.processTime != 0)
            {
                this.processTime = 0;
                notify = true;
            }
        }

        changed |= notify;

        if (changed)
        {
            markDirty();
        }

        if (notify)
        {
            IBlockState state = getWorld().getBlockState(getPos());
            getWorld().notifyBlockUpdate(getPos(), state, state, 3);
        }
    }

    public boolean canProcess(ItemStack recipeResult)
    {
        if (recipeResult.isEmpty())
        {
            return false;
        }
        else
        {
            ItemStack outputStack = outputSlot.getStackInSlot(0);
            if (outputStack.isEmpty())
            {
                return true;
            }
            else if (!outputStack.isItemEqual(recipeResult))
            {
                return false;
            }
            else if (outputStack.getCount() + recipeResult.getCount() <= outputSlot.getSlotLimit(0) && outputStack.getCount() + recipeResult.getCount() <= outputStack.getMaxStackSize())
            {
                return true;
            }
            else
            {
                return outputStack.getCount() + recipeResult.getCount() <= recipeResult.getMaxStackSize();
            }
        }
    }

    public void processItem(ElectricalBlastFurnaceRecipe recipe)
    {
        ItemStack outputStack = outputSlot.getStackInSlot(0).copy();
        int exp1 = (int) recipe.exp;

        if (Math.random() < recipe.exp - exp1)
        {
            exp1++;
        }

        while (exp1 > 0)
        {
            int s = EntityXPOrb.getXPSplit(exp1);
            exp1 -= s;
            getWorld().spawnEntity(new EntityXPOrb(getWorld(), getPos().getX() + 0.5D, getPos().getY() + 0.5D, getPos().getZ() + 0.5D, s));
        }

        if (outputStack.isEmpty())
        {
            outputStack = recipe.out.copy();
        }
        else if (outputStack.isItemEqual(recipe.out))
        {
            outputStack.grow(recipe.out.getCount());
        }
        outputSlot.setStackInSlot(0, outputStack);

        for (int i = 0; i < inputSlots.getSlots(); i++)
        {
            ItemStack stack = inputSlots.getStackInSlot(i).copy();
            if (!stack.isEmpty())
            {
                stack.shrink(1);
                inputSlots.setStackInSlot(i, stack);
            }
        }
    }
}
