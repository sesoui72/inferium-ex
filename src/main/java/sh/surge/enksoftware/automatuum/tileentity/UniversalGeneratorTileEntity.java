package sh.surge.enksoftware.automatuum.tileentity;

import sh.surge.enksoftware.automatuum.fluid.FluidRegistry;
import sh.surge.enksoftware.automatuum.util.MachineEnergyStorage;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SPacketUpdateTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ITickable;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.util.INBTSerializable;
import net.minecraftforge.energy.CapabilityEnergy;
import net.minecraftforge.energy.IEnergyStorage;
import net.minecraftforge.fluids.*;
import net.minecraftforge.fluids.capability.CapabilityFluidHandler;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.IItemHandlerModifiable;
import net.minecraftforge.items.ItemHandlerHelper;
import net.minecraftforge.items.ItemStackHandler;
import net.minecraftforge.items.wrapper.RangedWrapper;

import javax.annotation.Nullable;

public class UniversalGeneratorTileEntity extends TileEntity implements ITickable
{
    // Inheriting Generator TE's (Crude, Refined)
    public static class CrudeOilGeneratorTE extends UniversalGeneratorTileEntity
    {
        public CrudeOilGeneratorTE()
        {
            super(FluidRegistry.CRUDE_OIL, // Accepted fluid
                    10_000, // Energy capacity
                    2_000, // Tank capacity
                    10, // MB drained per tick
                    Fluid.BUCKET_VOLUME, // Minimum fluid for max energy rate
                    10, // Max Energy per MB
                    200.0F, // Minimum temp to start warming up
                    20 * 60, // Ticks to reach full warmup/max speed
                    2.0F, // Temp rise per tick while warm
                    1.0F, // Temp fall per tick while cool
                    1_000.0F, // explosion temp
                    3.0F // Explosion strength
            );
        }
    }

    public static class RefinedOilGeneratorTE extends UniversalGeneratorTileEntity
    {
        public RefinedOilGeneratorTE()
        {
            super(FluidRegistry.REFINED_OIL,
                    20_000,
                    4_000,
                    5,
                    Fluid.BUCKET_VOLUME,
                    20,
                    0.0F,
                    1,
                    0.0F,
                    0.0F,
                    9_999.0F,
                    0.0F
            );
        }
    }

    public final Fluid acceptedFluid;
    final int drainedMBperTick;
    final int maxEnergyRateMinFluid;
    final int maxEnergyPerMB;
    final float minWarmupTemp;
    final int maxWarmupTime;
    final float tempRisePerTick;
    final float tempCoolPerTick;
    public float explosionTemp;
    final float overheatExplosionStrength;
    public float temperature = 0.0F;
    public int elapsedTime = 0;
    MachineEnergyStorage energyStorage;
    public FluidTank fluidBuffer;

    IItemHandlerModifiable itemHandler = new ItemStackHandler(2)
    {
        @Override
        protected void onContentsChanged(int slot)
        {
            UniversalGeneratorTileEntity.this.markDirty();

            if (world != null)
            {
                IBlockState state = world.getBlockState(getPos());
                world.notifyBlockUpdate(getPos(), state, state, 3);
            }
        }
    };

    IItemHandlerModifiable inputSlot = new RangedWrapper(itemHandler, 0, 1);
    IItemHandlerModifiable outputSlot = new RangedWrapper(itemHandler, 1, 2);

    public UniversalGeneratorTileEntity(Fluid acceptedFluid, int energyCapacity, int tankCapacity, int drainedMBperTick, int maxEnergyRateMinFluid, int maxEnergyPerMB, float minWarmupTemp, int maxWarmupTime,
                                    float tempRisePerTick, float tempCoolPerTick, float explosionTemp, float explosionTempStrength)
    {
        this.acceptedFluid = acceptedFluid;
        this.energyStorage = new MachineEnergyStorage(energyCapacity, 0, energyCapacity);
        this.drainedMBperTick = drainedMBperTick;
        this.fluidBuffer = new FluidTank(tankCapacity)
        {
            @Override
            protected void onContentsChanged()
            {
                UniversalGeneratorTileEntity.this.markDirty();
            }

            @Override
            public boolean canFillFluidType(FluidStack fluid)
            {
                return fluid != null && fluid.getFluid() == UniversalGeneratorTileEntity.this.acceptedFluid;
            }

            @Override
            public boolean canDrain()
            {
                return false;
            }
        };

        this.maxEnergyRateMinFluid = maxEnergyRateMinFluid;
        this.maxEnergyPerMB = maxEnergyPerMB;
        this.minWarmupTemp = minWarmupTemp;
        this.maxWarmupTime = maxWarmupTime;
        this.tempRisePerTick = tempRisePerTick;
        this.tempCoolPerTick = tempCoolPerTick;
        this.explosionTemp = explosionTemp;
        this.overheatExplosionStrength = explosionTempStrength;
    }

    @Override
    public void update()
    {
        if (getWorld() == null || getWorld().isRemote)
        {
            return;
        }

        boolean changed = false;

        ItemStack container = itemHandler.getStackInSlot(0).copy();
        if (!container.isEmpty())
        {
            FluidActionResult emptiedSimulated = FluidUtil.tryEmptyContainer(container, fluidBuffer, fluidBuffer.getCapacity() - fluidBuffer.getFluidAmount(), null, false);
            if (emptiedSimulated.isSuccess())
            {
                ItemStack remainder = ItemHandlerHelper.insertItemStacked(outputSlot, emptiedSimulated.getResult(), true);
                if (remainder.isEmpty())
                {
                    FluidActionResult emptiedReal = FluidUtil.tryEmptyContainer(container, fluidBuffer, fluidBuffer.getCapacity() - fluidBuffer.getFluidAmount(), null, true);
                    remainder = ItemHandlerHelper.insertItemStacked(outputSlot, emptiedReal.getResult(), false);

                    ItemStack containerCopy = container.copy();
                    containerCopy.shrink(1);
                    itemHandler.setStackInSlot(0, containerCopy);
                }
                changed = true;
            }
        }

        if (temperature >= minWarmupTemp)
        {
            if (elapsedTime < maxWarmupTime)
            {
                elapsedTime++;
                changed = true;
            }
        }
        else if (elapsedTime > 0)
        {
            elapsedTime--;
            changed = true;
        }

        // If there's enough fluid to be used and space for energy
        if (fluidBuffer.getFluidAmount() >= drainedMBperTick && energyStorage.getEnergyStored() < energyStorage.getMaxEnergyStored())
        {
            final int energy = (int)((Math.min(fluidBuffer.getFluidAmount(), maxEnergyRateMinFluid) / (float)maxEnergyRateMinFluid) * drainedMBperTick * maxEnergyPerMB * (elapsedTime / (float)maxWarmupTime));
            energyStorage.setEnergy(Math.min(energyStorage.getMaxEnergyStored(), energyStorage.getEnergyStored() + energy));
            fluidBuffer.drainInternal(drainedMBperTick, true);
            temperature += tempRisePerTick;

            changed = true;
        }
        else if (temperature > 0.0F) // Not enough fluid left or energy full
        {
            temperature -= tempCoolPerTick;
            changed = true;
        }
        else if (temperature < 0.0F)
        {
            temperature = 0.0F;
            changed = true;
        }

        // Push energy to adjacent energy acceptors (Untested)
        for (EnumFacing facing : EnumFacing.VALUES)
        {
            TileEntity te = getWorld().getTileEntity(getPos().offset(facing));
            if (te != null && te.hasCapability(CapabilityEnergy.ENERGY, facing.getOpposite()))
            {
                IEnergyStorage destination = te.getCapability(CapabilityEnergy.ENERGY, facing.getOpposite());
                destination.receiveEnergy(energyStorage.extractEnergy(destination.receiveEnergy(Integer.MAX_VALUE, true), false), false);
                changed = true;
            }
        }

        if (temperature > explosionTemp)
        {
            temperature = 0.0F;
            BlockPos pos = getPos();
            getWorld().createExplosion(null, pos.getX(), pos.getY(), pos.getZ(), overheatExplosionStrength, true);
            changed = true;
        }

        if (changed)
        {
            markDirty();
            IBlockState state = getWorld().getBlockState(getPos());
            getWorld().notifyBlockUpdate(getPos(), state, state, 3);
        }
    }

    @Nullable
    @Override
    public SPacketUpdateTileEntity getUpdatePacket()
    {
        return new SPacketUpdateTileEntity(getPos(), 0, getUpdateTag());
    }

    @Override
    public void onDataPacket(NetworkManager net, SPacketUpdateTileEntity pkt)
    {
        handleUpdateTag(pkt.getNbtCompound());
    }

    @Override
    public void handleUpdateTag(NBTTagCompound tag)
    {
        super.handleUpdateTag(tag);
        fluidBuffer.readFromNBT(tag);
        temperature = tag.getFloat("temp");
    }

    @Override
    public NBTTagCompound getUpdateTag()
    {
        NBTTagCompound tag = super.getUpdateTag();
        tag = fluidBuffer.writeToNBT(tag);
        tag.setFloat("temp", temperature);
        tag.setTag("energy", CapabilityEnergy.ENERGY.writeNBT(energyStorage, null));
        return tag;
    }

    @Override
    public void readFromNBT(NBTTagCompound compound)
    {
        super.readFromNBT(compound);
        if (compound.hasKey("inventory") && itemHandler instanceof INBTSerializable<?>)
        {
            ((INBTSerializable<NBTTagCompound>) itemHandler).deserializeNBT(compound.getCompoundTag("inventory"));
        }
        fluidBuffer.readFromNBT(compound);
        temperature = compound.getFloat("temp");
        elapsedTime = compound.getInteger("eTime");
        CapabilityEnergy.ENERGY.readNBT(energyStorage, null, compound.getTag("energy"));
    }

    @Override
    public NBTTagCompound writeToNBT(NBTTagCompound compound)
    {
        super.writeToNBT(compound);
        if (itemHandler instanceof INBTSerializable<?>)
        {
            compound.setTag("inventory", ((INBTSerializable<?>) itemHandler).serializeNBT());
        }
        fluidBuffer.writeToNBT(compound);
        compound.setFloat("temp", temperature);
        compound.setInteger("eTime", elapsedTime);
        compound.setTag("energy", CapabilityEnergy.ENERGY.writeNBT(energyStorage, null));
        return compound;
    }

    @Override
    public boolean hasCapability(Capability<?> capability, @Nullable EnumFacing facing)
    {
        return capability == CapabilityEnergy.ENERGY ||
                capability == CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY ||
                capability == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY ||
                super.hasCapability(capability, facing);
    }

    @Nullable
    @Override
    public <T> T getCapability(Capability<T> capability, @Nullable EnumFacing facing)
    {
        if (capability == CapabilityEnergy.ENERGY)
        {
            return CapabilityEnergy.ENERGY.cast(energyStorage);
        }
        else if (capability == CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY)
        {
            return CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY.cast(fluidBuffer);
        }
        else if (capability == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY)
        {
            return CapabilityItemHandler.ITEM_HANDLER_CAPABILITY.cast(
                    facing == null ? itemHandler : facing == EnumFacing.DOWN ? outputSlot : inputSlot
            );
        }
        return super.getCapability(capability, facing);
    }

    // Interaction
    public boolean canInteractWith(EntityPlayer playerIn)
    {
        return !isInvalid() && playerIn.getDistanceSq(pos.add(0.5D, 0.5D, 0.5D)) <= 64D;
    }
}
