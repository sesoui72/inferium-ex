package sh.surge.enksoftware.automatuum.tileentity;

import sh.surge.enksoftware.automatuum.util.MachineEnergyStorage;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SPacketUpdateTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ITickable;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.energy.CapabilityEnergy;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.IItemHandlerModifiable;
import net.minecraftforge.items.ItemStackHandler;
import net.minecraftforge.items.wrapper.RangedWrapper;

import javax.annotation.Nullable;
import java.util.Optional;

public class OilRefinerTileEntity extends TileEntity implements ITickable
{
    // Fields
    IItemHandlerModifiable itemHandler = new ItemStackHandler(3)
    {
        @Override
        protected void onContentsChanged(int slot)
        {
            OilRefinerTileEntity.this.markDirty();

            Optional.ofNullable(getWorld()).ifPresent(world ->
            {
                IBlockState state = world.getBlockState(getPos());
                world.notifyBlockUpdate(getPos(), state, state, 3);
            });
        }
    };

    MachineEnergyStorage energyStorage = new MachineEnergyStorage(5000, 500, 0);
    IItemHandlerModifiable inputSlots = new RangedWrapper(itemHandler, 0, 2);
    IItemHandlerModifiable outputSlot = new RangedWrapper(itemHandler, 2, 3);

    public int processTime = 0;
    public int totalProcessTime = 0;

    @Nullable
    @Override
    public SPacketUpdateTileEntity getUpdatePacket()
    {
        return new SPacketUpdateTileEntity(getPos(), 0, getUpdateTag());
    }

    @Override
    public void onDataPacket(NetworkManager net, SPacketUpdateTileEntity pkt)
    {
        handleUpdateTag(pkt.getNbtCompound());
    }

    @Override
    public NBTTagCompound getUpdateTag()
    {
        NBTTagCompound tag = super.getUpdateTag();
        tag.setTag("energy", CapabilityEnergy.ENERGY.writeNBT(energyStorage, null));
        tag.setFloat("processTime", processTime);
        tag.setFloat("totalProcessTime", totalProcessTime);
        return tag;
    }

    @Override
    public void readFromNBT(NBTTagCompound compound)
    {
        super.readFromNBT(compound);
        if (compound.hasKey("inventory"))
        {
            CapabilityItemHandler.ITEM_HANDLER_CAPABILITY.readNBT(itemHandler, null, compound.getTag("inventory"));
        }
        CapabilityEnergy.ENERGY.readNBT(energyStorage, null, compound.getTag("energy"));
        processTime = compound.getInteger("processTime");
        totalProcessTime = compound.getInteger("totalProcessTime");
    }

    @Override
    public NBTTagCompound writeToNBT(NBTTagCompound compound)
    {
        compound = super.writeToNBT(compound);
        compound.setTag("inventory", CapabilityItemHandler.ITEM_HANDLER_CAPABILITY.writeNBT(itemHandler, null));
        compound.setTag("energy", CapabilityEnergy.ENERGY.writeNBT(energyStorage, null));
        compound.setInteger("processTime", processTime);
        compound.setInteger("totalProcessTime", totalProcessTime);
        return compound;
    }

    @Override
    public boolean hasCapability(Capability<?> capability, @Nullable EnumFacing facing)
    {
        return capability == CapabilityEnergy.ENERGY ||
                capability == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY ||
                super.hasCapability(capability, facing);
    }

    @Nullable
    @Override
    public <T> T getCapability(Capability<T> capability, @Nullable EnumFacing facing)
    {
        if (capability == CapabilityEnergy.ENERGY)
        {
            return CapabilityEnergy.ENERGY.cast(energyStorage);
        }
        else if (capability == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY)
        {
            return CapabilityItemHandler.ITEM_HANDLER_CAPABILITY.cast(
                    facing == null ? itemHandler : facing == EnumFacing.DOWN ? outputSlot : inputSlots
            );
        }
        return super.getCapability(capability, facing);
    }

    public boolean canInteractWith(EntityPlayer player)
    {
        return !isInvalid() && player.getDistanceSq(pos.add(0.5D, 0.5d, 0.5D)) <= 64;
    }

    @Override
    public void update()
    {
        if (getWorld() == null || getWorld().isRemote)
        {
            return;
        }

        boolean changed = false;
        boolean notify = false;

        // Am stumped.

        changed |= notify;

        if (changed)
        {
            markDirty();
        }

        if (notify)
        {
            IBlockState state = getWorld().getBlockState(getPos());
            getWorld().notifyBlockUpdate(getPos(), state, state, 3);
        }
    }

    // Custom voids
}
